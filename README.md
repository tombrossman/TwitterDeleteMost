# TwitterDeleteMost
TwitterDeleteMost is a small application to delete your Twitter activity, based on a number of variables which you can configure.

## Features
- Delete, unfavorite and unretweet tweets
- Keep tweets based on age, retweet, favourite count, or specific tweet ID.

## Usage
To setup locally run:
```bash
git clone https://gitlab.com/tombrossman/TwitterDeleteMost
cd TwitterDeleteMost
bundle install
```

Get the Twitter API variables from https://apps.twitter.com and add the following variables to a `.env` file in the `TwitterDelete` folder:
```bash
TWITTER_CONSUMER_KEY=...
TWITTER_CONSUMER_SECRET=...
TWITTER_ACCESS_TOKEN=...
TWITTER_ACCESS_TOKEN_SECRET=...
```

Now run TwitterDeleteMost:
```bash
./twitter_delete_most.rb --user TwitterUsername
```

## Status
Tested and working.

## License
TwitterDeleteMost is licensed under the [AGPLv3 License](https://en.wikipedia.org/wiki/Affero_General_Public_License).
The full license text is available in [LICENSE.txt](https://gitlab.com/tombrossman/TwitterDeleteMost/blob/master/LICENSE.txt).
